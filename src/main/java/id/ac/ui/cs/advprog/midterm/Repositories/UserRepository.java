package id.ac.ui.cs.advprog.midterm.Repositories;

import id.ac.ui.cs.advprog.midterm.Entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
