package id.ac.ui.cs.advprog.midterm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication

public class MidtermProgrammingExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(MidtermProgrammingExamApplication.class, args);
	}

}
